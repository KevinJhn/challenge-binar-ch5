const store = require("store");

const authorize = (req, res, next) => {
    let a = store.get("username") || "none"
    if (a != "none") {
        if (a.userName == "kevin") {
            next()
            return
        } else {
            return res.render("login")
        }
    } else {
        return res.render("login")
    }
}
module.exports = authorize