const path = require("path")
const express = require("express")
const fs = require("fs")
const authorize = require("./authorize")
const store = require("store")
const app = express()
const port = 8000
app.use(express.json())

app.use(express.static(path.join(__dirname, "public")))
app.use(express.urlencoded({
    extended: true
}))

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    let a = store.get("username") || "login"
    if (a != "login") {
        a = a.userName
    }
    res.render("home", {
        username: a
    })
})

app.get('/login', (req, res) => {
    res.render("login")
})

app.get('/suit', [authorize], (req, res) => {
    res.render("suit")
})

app.post("/login", (req, res) => {
    fs.readFile("./static.json", (err, data) => {
        let d = JSON.parse(data)
        if (req.body["username"] == d["username"] && req.body["password"] == d["password"]) {
            store.set("username", {
                userName: d["username"]
            })
            res.redirect("/")
        } else {
            res.status(500).send({
                "error": "wrong username/password"
            })
        }
    })
})

app.listen(port, () => console.log(port))